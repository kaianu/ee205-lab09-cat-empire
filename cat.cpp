///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file cat.cpp
/// @version 1.0
///
/// Exports data about all cats
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <cassert>
#include <random>
#include <queue>
#include <iostream>
#include <string>

#define CAT_NAMES_FILE "names.txt"

#include "cat.hpp"

using namespace std;

Cat::Cat( const std::string newName ) {
	setName( newName );
}


void Cat::setName( const std::string newName ) {
	assert( newName.length() > 0 );

	name = newName;
}


std::vector<std::string> Cat::names;


void Cat::initNames() {
	names.clear();
	cout << "Loading... ";

	ifstream file( CAT_NAMES_FILE );
	string line;
	while (getline(file, line)) names.push_back(line);

	cout << to_string( names.size() ) << " names." << endl;
}


Cat* Cat::makeCat() {
	// Get a random cat from names
	auto nameIndex = rand() % names.size();
	auto name = names[nameIndex];

	// Remove the cat (by index) from names
	erase( names, names[nameIndex] );

	return new Cat( name );
}


void CatEmpire::catFamilyTree() const{
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

	dfsInorderReverse( topCat, 1 );
}

void CatEmpire::catList() const{
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

	dfsInorder( topCat );
}

void CatEmpire::catBegat() const{
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

   dfsPreorder( topCat );
}

bool CatEmpire::empty() {
   if(topCat == nullptr) {
      return true;
   }
   catFamilyTree();
   catList();
   catBegat();
   //catGenerations();
   return false;
}

void CatEmpire::addCat(Cat* newCat){
   if(empty() == true){
      topCat = newCat;
   }
   else{
      addCat(topCat, newCat);
   }
}

void CatEmpire::addCat(Cat* atCat, Cat* newCat) {
   if( atCat->name > newCat->name){
      if(atCat->left == nullptr){
         atCat->left = newCat;
      }else{
         addCat( atCat->left, newCat);
      }
   }
}

void CatEmpire::dfsInorderReverse(Cat* atCat, int depth) const{
   if( atCat->right != nullptr){
      dfsInorderReverse(atCat->right, depth + 1);
   }
   cout << string (5  * (depth-1),' ') << atCat->name;
   
   if( atCat->left == nullptr){
      dfsInorderReverse(atCat->left, depth + 1);
   }

}

void CatEmpire::dfsInorder(Cat* atCat) const{
   if( atCat->right != nullptr){
      dfsInorder(atCat->left);
   }
   cout << atCat->name;

   if( atCat->left == nullptr){
      dfsInorder(atCat->right);
   }
  
}

void CatEmpire::dfsPreorder(Cat* atCat) const{
   cout << atCat-> name;
   if( atCat->left != nullptr){
      dfsPreorder(atCat->left);
   }

   if( atCat->right == nullptr){
      dfsPreorder(atCat->right);
   }
}

void CatEmpire::catGenerations() const{
   
  /* if( topCat == nullptr){
      return;
   }
   queue<Cat*> catQueue;
   
   catQueue.push(topCat);

   for(int i = 0; i < catQueue.size(); i++){
      if(catQueue[i]->left != nullptr){
         catQueue.push(catQueue[i]->left);
       }
      if(catQueue[i]->right != nullptr){
         catQueue.push(catQueue[i]->right);
      }
   }
   for (int i = 0; i < catQueue.size(); i++){
      cout << catQueue.pop();
   }
   */
}
